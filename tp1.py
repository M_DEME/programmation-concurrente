import math
# exercice 1
def isocele(a,b,c):
    if a == b or a == c or b == c:
        if (a == 1 and b == 1 and c ==20) or (a == 1 and b == 20 and c ==1) or (a == 20 and b == 1 and c ==1):
           return False
        else:
            return True
    else:
           return False

# exercice 2
def aire_ordonne(a,b,c):
    u1 = min(a,b,c)
    u3 = max(a,b,c)
    u2 = (a+b+c)-(u1+u3)
    return  math.sqrt((u1**2*u3**2)-((u1**2-u2**2+u3**2)/2)**2)/2

#exercice 3
def definit_triangle(a,b,c):
    if (a<=0 or b<=0 or c<=0) or ( a>b+c or b>a+c or c>a+b):
        return False
    return True

def nb_triangles_speciaux(n,p):
    cpt = 0
    for i in range(n,p+1):
      for j in range(n,p+1):
        for k in range(n,p+1):
            if(definit_triangle(i,j,k)==True and aire_ordonne(i,j,k)==(i+j+k)):
                cpt+=1
    return cpt/6
# affichage de l'exercice 1
print("output de l'exercice 1")
print(isocele(4,2,3))
print(isocele(4,3,3))
print(isocele(4,4,4))
print("fin exo 1")
#affichage de l'exercice 2
print(" output de l'exercice 2")
print(aire_ordonne(4,2,3))
print(aire_ordonne(4,3,3))
print(aire_ordonne(3,4,5))
print(aire_ordonne(13,14,15))
print(aire_ordonne(1,1,1))
print("fin de l'exercice 2")

#affichage de l'exercice 3
print("ouput de l'exercice 3")
print(definit_triangle(1,1,20))
print(definit_triangle(4,2,3))
print(definit_triangle(4,4,4))
print("fin de l'exercice 3")
#affichage de l'exercice 4
print("output de l'exercice 4")
print(nb_triangles_speciaux(1,20))
print("fin de l'exercice 4")