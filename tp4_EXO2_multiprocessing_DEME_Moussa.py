from multiprocessing import Lock, Process
import random
import sys
import time

def calcul_square(nombres):
    nombreCarree = []
    for i in nombres:
        nombreCarree.append(i ** 2)
    return nombreCarree

def calcul_cube(nombres):
    nombresCube = []
    for i in nombres:
        nombresCube.append(i ** 3)
    return nombresCube

def task_with_verrou(lock,tab):
    lock.acquire()
    try:
        i = 0
        while i < 3:
           for elem in tab:
               text = str(elem) + "\n"
               sys.stdout.write(text)
               sys.stdout.flush()
               attente = 0.2
               attente += random.randint(1, 60) / 100
               time.sleep(attente)
           i += 1
    finally:
       lock.release()

def main():
    """ main program """
    lock = Lock()
    tab = [calcul_square([2,3,8,9,12]),calcul_cube([2,3,8,9,12])]
    processes = []

    for elem in tab:
        proc = Process(target=task_with_verrou, args=(lock,elem))
        processes.append(proc)
        proc.start()

    for proc in processes:
        proc.join()


if __name__ == "__main__":
    main()