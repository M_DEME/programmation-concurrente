from math import pow
import time

#xercice 1
def comptes_mots(ch):
    text = ch.split(" ")
    taille = len(text)
    if(text[0]==""):
        taille-=1
    if (text[len(text)-1] == ""):
        taille -= 1
    return taille

#exercice 2
def remplace_multiple(s1,s2,n):
    i = 0
    j = 0
    s1 = list(s1)
    for i in range(len(s2)):
        if((n+j)<len(s1)):
            s1[n + j] = s2[i]
            j = j+n
        else:
            break
    s2 = list(s2)
    s1 = s1+s2[i:(len(s2)-1)]
    s1 = ' '.join(s1)
    print(s1)


#enoncé 1
def termeU(n):
    if n == 0:
        return 1
    else:
        return termeU(n-1)*(pow(2,n))+n
#enoncé 2
def serie(n):
    s = 0
    for i in range(n+1):
        s += termeU(i)
    return s
#enoncé 3
def serie_V2(n):
    s = 0
    for i in range(n+1):
        if i == 0:
           u = 1
        else:
           u = u*(pow(2,i))+i
        s +=u
    return s

#enoncé 4
"""
remplir à chaque iteration les valeurs des variables pour la fonction serie_v2

1) u = 1 et s = 1
2) u = 3 et s+4
"""
def temps(ch,n):
    depart = time.perf_counter()
    ch(n)
    arrivee = time.perf_counter()
    print("temps passé en seconde : ",arrivee-depart)
#exercice 4
#version recusive
def fact(n):
    if(n==1):
        return 1
    return n*fact(n-1)

#version iterative
def factIterative(n):
    fact = 1
    for i in range(n):
        fact=fact*(i+1)
    return fact
print("test exo 1")
print("dans cette phrase on a : ",comptes_mots(" je suis  moussa deme et je suis etudiant en M1 info ")," mots")
print("test exercice 2")
remplace_multiple("hirondelles","nid",3)
print("terme U test")
print(termeU(0))
print(termeU(1))
print(termeU(5))
print(termeU(10))
print("serie test")
print(serie(0))
print(serie(1))
print(serie(5))
print(serie(10))
print("serie_V2 test")
print(serie_V2(0))
print(serie_V2(1))
print(serie_V2(5))
print(serie_V2(10))

print(temps(serie,100))
print(temps(serie_V2,100))
print(temps(serie,300))
print(temps(serie_V2,300))
""" le plus performant est le test avec la serie_V2"""

print("exercice 4 factorielle")
print(fact(4))
print(factIterative(5))
print(fact(6))
print(factIterative(7))