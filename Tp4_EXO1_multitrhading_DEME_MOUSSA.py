import random
import sys
from threading import Thread, RLock
import time

verrou = RLock()
class Tp4(Thread):
    def __init__(self,tab):
        Thread.__init__(self)
        self.tab = tab
    def calcul_square(nombres):
        nombreCarree = []
        for i in nombres:
            nombreCarree.append(i ** 2)
        return nombreCarree

    def calcul_cube(nombres):
        nombresCube = []
        for i in nombres:
            nombresCube.append(i**3)
        return nombresCube


    def run(self):
        """code a executer pendant l'execution du threads"""
        i = 0
        while i < 5:
          with verrou:
            for elem in self.tab:
                text = str(elem)+"\n"
                sys.stdout.write(text)
                sys.stdout.flush()
                attente = 0.2
                attente += random.randint(1, 60) / 100
                time.sleep(attente)
            i += 1
if __name__=="__main__":
#creation des threads

 threads_1 = Tp4(Tp4.calcul_square([2,3,8,9,12]))
 threads_2 = Tp4(Tp4.calcul_cube([2,3,8,9,12]))

 #lancement des threads
 threads_1.start()
 threads_2.start()

 #attend que les threads se terminent
 threads_1.join()
 threads_2.join()